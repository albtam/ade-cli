.. _mounting-ports:

Mounting ports to ADE
^^^^^^^^^^^^^^^^^^^^^

Use the ``ADDARGS`` arguments of ``ade start``. See :ref:`addargs` for details.
